#!/usr/bin/env python3
# -*- coding: utf-8 -*-

##########################################################################
#    TP - Introduction à l'interpolation spatiale et aux géostatistiques #
##########################################################################

# P. Bosser / ENSTA Bretagne
# Version du 26/03/2017


# Numpy
import numpy as np
# Matplotlib / plot
import matplotlib.pyplot as plt

import scipy.spatial as sci

################## Modèle de fonction d'interpolation ##################

def interp_inv(x_obs, y_obs, z_obs, x_int, y_int, p):
    # Interpolation par ???
    # x_obs, y_obs, z_obs : observations
    # [np.array dimension 1*n]
    # x_int, y_int, positions pour lesquelles on souhaite interpoler une valeur z_int
    # [np array dimension m*p]
    #p : la puissance à laquelle on souhaite faire l'interpolation 
    
    z_int = np.nan*np.zeros(x_int.shape)
    for i in np.arange(0,x_int.shape[0]):
        for j in np.arange(0,x_int.shape[1]):
            
            s_poids = np.sum(1/np.sqrt((x_int[i,j]-x_obs)**p+(y_int[i,j]-y_obs)**p)**1/p)
            s_obs = np.sum(z_obs/np.sqrt((x_int[i,j]-x_obs)**p+(y_int[i,j]-y_obs)**p)**1/p)
            
            z_int[i,j] = s_obs/s_poids
            
    return z_int

####################### Fonctions d'interpolation ######################

def interp_sfc(x_obs, y_obs, z_obs, x_int, y_int):
    # Interpolation par surface de tendance
    # x_obs, y_obs, z_obs : observations
    # [np.array dimension 1*n]
    # x_int, y_int, positions pour lesquelles on souhaite interpoler une valeur z_int
    # [np array dimension m*p]
    
    z_int = np.nan*np.zeros(x_int.shape)
    
    return z_int

def interp_lin(x_obs, y_obs, z_obs, x_int, y_int):
    # Interpolation par triangulation de Delaunay
    # x_obs, y_obs, z_obs : observations
    # [np.array dimension 1*n]
    # x_int, y_int, positions pour lesquelles on souhaite interpoler une valeur z_int
    # [np array dimension m*p]
    
    z_int = np.nan*np.zeros(x_int.shape)
    tr_delaunay = sci.Delaunay(np.hstack( (x_obs, y_obs) ))
    for i in np.arange(0,x_int.shape[0]):
        for j in np.arange(0,x_int.shape[1]):
            #Récupération du triangle correspondant
            idx_t = tr_delaunay.find_simplex( np.array([x_int[i,j], y_int[i,j]]) )
            if(idx_t == -1):
                continue
            
            #Récupération des sommets du triangle correspondant
            idx_s = tr_delaunay.simplices[idx_t,:]
            x1 = x_obs[ idx_s[0] ][0]
            y1 = y_obs[ idx_s[0] ][0]
            z1 = z_obs[idx_s[0]][0]
            x2 = x_obs[ idx_s[1] ][0] 
            y2 = y_obs[ idx_s[1] ][0]
            z2 = z_obs[idx_s[1]][0]
            x3 = x_obs[ idx_s[2] ][0] 
            y3 = y_obs[ idx_s[2] ][0]
            z3 = z_obs[idx_s[2]][0]
            #Détermination et résolution du système
            X = np.array([[x1,y1,1],[x2,y2,1],[x3,y3,1]])
            Y = np.array([z1,z2,z3])
            coef = np.linalg.solve(X,Y)
            #Affectation de la valeur par combinaison linéaire
            z_int[i,j] = coef[0]*x_int[i,j]+coef[1]*y_int[i,j]+coef[2]
    return z_int
    
def interp_ppv(x_obs, y_obs, z_obs, x_int, y_int):
    # Interpolation par plus proche voisin
    # x_obs, y_obs, z_obs : observations
    # [np.array dimension 1*n]
    # x_int, y_int, positions pour lesquelles on souhaite interpoler une valeur z_int
    # [np array dimension m*p]
    
    z_int = np.nan*np.zeros(x_int.shape)
    for i in np.arange(0,x_int.shape[0]):
        for j in np.arange(0,x_int.shape[1]):
            z_int[i,j] = z_obs[np.argmin(np.sqrt((x_int[i,j]-x_obs)**2+(y_int[i,j]-y_obs)**2))]
    return z_int

############################# Méthode stochastique ############################
    
def recup_ntil(distance,h,epsilon):
    #
    #
    #
    #
    
    return np.where((distance!=0) & (distance>h-epsilon) & (distance<h+epsilon) )
    
def variog_empirique(x_obs, y_obs, z_obs, x_int, y_int):
    #
    #
    #
    #
    
    ecart_carre = np.nan*np.zeros((x_obs.shape[0],x_obs.shape[0]))
    distance = np.nan*np.zeros((x_obs.shape[0],x_obs.shape[0]))
    for i in np.arange(0,x_obs.shape[0]):
        for j in np.arange(0,x_obs.shape[0]):
            ecart_carre[i,j] = 0.5*(z_obs[i,0]-z_obs[j,0])**2
            distance[i,j] = np.sqrt((x_obs[i,0]-x_obs[j,0])**2+(y_obs[i,0]-y_obs[j,0])**2)
    

    
    
    """
    grille régulière des distances à étudier successivement :
    h = 5, puis h = 10 etc... jusquà 205 je crois
    """
    mini_h = 5
    maxi_h = int(np.floor(np.max(distance)))
    pas_h = 5
    lst_h = np.linspace(mini_h,maxi_h,int((maxi_h-mini_h)/pas_h) + 1)
    print(lst_h)
    
    """
    caractérise le "environ =" à h : on cherchera les couples de
    points séparés d'une distance inclue dans [h-epsilon,h+epsilon]
    [2.5,7.5] puis [7.5,12.5] etc...
    """
    epsilon = 2.5
    gamma = np.array([])
    
    for h in lst_h:
        
        #récupération des couples inclut dans l'intervalle de distance
        ntil = recup_ntil(distance,h,epsilon)
        card_ntil = ntil[0].shape[0]
        if(card_ntil == 0):
            continue
    
        somme_nuee_class = 0
        
        #calcul de la nuée variographique sur cet intervalle
        for i in np.arange(0,card_ntil):
            somme_nuee_class += ecart_carre[ntil[0][i],ntil[1][i]]
        gamma = np.hstack((gamma,np.array([(1/card_ntil)*somme_nuee_class])))

    return gamma, lst_h, ecart_carre, distance
    
    

############################# Visualisation ############################

def plot_contour_2d(x_grd ,y_grd ,z_grd, x_obs = np.array([]) ,y_obs = np.array([]), xlabel = "", ylabel = "", title = "", fileo = ""):
    # Tracé du champ interpolé sous forme d'isolignes
    # x_grd, y_grd, z_grd : grille de valeurs interpolées
    # x_obs, y_obs : observations (facultatif)
    # xlabel, ylabel : étiquettes des axes (facultatif)
    # title : titre (facultatif)
    # fileo : nom du fichier d'enregistrement de la figure    (facultatif)
    
    z_grd_m = np.ma.masked_invalid(z_grd)
    fig = plt.figure()
    plt.contour(x_grd, y_grd, z_grd_m, int(np.round((np.max(z_grd_m)-np.min(z_grd_m))/4)),colors ='k', stride=1)
    if x_obs.shape[0]>0:
        plt.scatter(x_obs, y_obs, marker = 'o', c = 'k', s = 5)
        plt.xlim(0.95*np.min(x_obs),np.max(x_obs)+0.05*np.min(x_obs))
        plt.ylim(0.95*np.min(y_obs),np.max(y_obs)+0.05*np.min(y_obs))
    else:
        plt.xlim(0.95*np.min(x_grd),np.max(x_grd)+0.05*np.min(x_grd))
        plt.ylim(0.95*np.min(y_grd),np.max(y_grd)+0.05*np.min(y_grd))
    plt.gca().set_aspect('equal', adjustable='box')
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.title(title)
    plt.grid()
    if not fileo == "": plt.savefig(fileo,bbox_inches='tight')

def plot_surface_2d(x_grd ,y_grd ,z_grd, x_obs = np.array([]) ,y_obs = np.array([]), minmax = [0,0], xlabel = "", ylabel = "", zlabel = "", title = "", fileo = ""):
    # Tracé du champ interpolé sous forme d'une surface colorée
    # x_grd, y_grd, z_grd : grille de valeurs interpolées
    # x_obs, y_obs : observations (facultatif)
    # minmax : valeurs min et max de la variable interpolée (facultatif)
    # xlabel, ylabel, zlabel : étiquettes des axes (facultatif)
    # title : titre (facultatif)
    # fileo : nom du fichier d'enregistrement de la figure    (facultatif)
    
    from matplotlib import cm
    
    z_grd_m = np.ma.masked_invalid(z_grd)
    fig = plt.figure()
    if minmax[0] < minmax[-1]:
        p=plt.pcolormesh(x_grd, y_grd, z_grd_m, cmap=cm.terrain, vmin = minmax[0], vmax = minmax[-1])
    else:
        p=plt.pcolormesh(x_grd, y_grd, z_grd_m, cmap=cm.terrain)
    if x_obs.shape[0]>0:
        plt.scatter(x_obs, y_obs, marker = 'o', c = 'k', s = 5)
        plt.xlim(0.95*np.min(x_obs),np.max(x_obs)+0.05*np.min(x_obs))
        plt.ylim(0.95*np.min(y_obs),np.max(y_obs)+0.05*np.min(y_obs))
    else:
        plt.xlim(0.95*np.min(x_grd),np.max(x_grd)+0.05*np.min(x_grd))
        plt.ylim(0.95*np.min(y_grd),np.max(y_grd)+0.05*np.min(y_grd))
    plt.gca().set_aspect('equal', adjustable='box')
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.title(title)
    plt.grid()
    fig.colorbar(p,ax=plt.gca(),label=zlabel,fraction=0.046, pad=0.04)
    if not fileo == "": plt.savefig(fileo,bbox_inches='tight')

def plot_points(x_obs, y_obs, xlabel = "", ylabel = "", title = "", fileo = ""):
    # Tracé des sites d'observations
    # x_obs, y_obs : observations
    # xlabel, ylabel : étiquettes des axes (facultatif)
    # title : titre (facultatif)
    # fileo : nom du fichier d'enregistrement de la figure    (facultatif)
    
    fig = plt.figure()
    ax = plt.gca()
    plt.plot(x_obs, y_obs, 'ok', ms = 4)
    ax.set_xlim(0.95*min(x_obs),max(x_obs)+0.05*min(x_obs))
    ax.set_ylim(0.95*min(y_obs),max(y_obs)+0.05*min(y_obs))
    ax.set_aspect('equal', adjustable='box')
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.title(title)
    plt.grid()
    if not fileo == "": plt.savefig(fileo,bbox_inches='tight')

def plot_patch(x_obs, y_obs, z_obs, xlabel = "", ylabel = "", zlabel = "", title = "", fileo = ""):
    # Tracé des valeurs observées
    # x_obs, y_obs, z_obs : observations
    # xlabel, ylabel, zlabel : étiquettes des axes (facultatif)
    # title : titre (facultatif)
    # fileo : nom du fichier d'enregistrement de la figure    (facultatif)
    
    from matplotlib import cm
    
    fig = plt.figure()
    p=plt.scatter(x_obs, y_obs, marker = 'o', c = z_obs, s = 80, cmap=cm.terrain)
    plt.xlim(0.95*min(x_obs),max(x_obs)+0.05*min(x_obs))
    plt.ylim(0.95*min(y_obs),max(y_obs)+0.05*min(y_obs))
    plt.gca().set_aspect('equal', adjustable='box')
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.title(title)
    plt.grid()
    fig.colorbar(p,ax=plt.gca(),label=zlabel,fraction=0.046, pad=0.04)
    if not fileo == "": plt.savefig(fileo,bbox_inches='tight')

def plot_triangulation(x_obs, y_obs, xlabel = "", ylabel = "", title = "", fileo = ""):
    # Tracé de la triangulation sur des sites d'observations
    # x_obs, y_obs : observations
    # xlabel, ylabel : étiquettes des axes (facultatif)
    # title : titre (facultatif)
    # fileo : nom du fichier d'enregistrement de la figure    (facultatif)
    from scipy.spatial import Delaunay as delaunay
    tri = delaunay(np.hstack((x_obs,y_obs)))
    
    plt.figure()
    plt.triplot(x_obs[:,0], y_obs[:,0], tri.simplices)
    plt.plot(x_obs, y_obs, 'or', ms=4)
    plt.xlim(0.95*min(x_obs),max(x_obs)+0.05*min(x_obs))
    plt.ylim(0.95*min(y_obs),max(y_obs)+0.05*min(y_obs))
    plt.gca().set_aspect('equal', adjustable='box')
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.title(title)
    plt.grid()
    if not fileo == "": plt.savefig(fileo,bbox_inches='tight')
    
def plot_variogramme(lst_h, gamma_h, ecart_carres, dist_ecart_carres):
    
    plt.figure()
    plt.plot(dist_ecart_carres, ecart_carres, "bo")
    plt.plot(lst_h,gamma_h, color = "red")
    plt.xlim(0.95*min(dist_ecart_carres),max(dist_ecart_carres)+0.05*min(dist_ecart_carres))
    plt.ylim(0.95*min(ecart_carres),max(ecart_carres)+0.05*min(ecart_carres))
    plt.grid()
    
################################## Main ################################

#def main():
    

if __name__ == "__main__":
    
    #main()
    
    # Chargement des données
    data = np.loadtxt('points.dat')
    # On force le format des données (matrice d'une colonne)
    x_obs = data[:,0].reshape((-1, 1))
    y_obs = data[:,1].reshape((-1, 1))
    z_obs = data[:,2].reshape((-1, 1))
    
     
    
    # Visualisation des données en entrée
    #plot_points(x_obs, y_obs, xlabel = 'x [m]', ylabel = 'y [m]', title = "sites d'observation")
    #plot_patch(x_obs, y_obs, z_obs, xlabel = 'x [m]', ylabel = 'y [m]', zlabel = 'z [m]', title = "observations")
    
    # Création d'une grille pour l'interpolation
    x_grd, y_grd = np.meshgrid(np.linspace(np.floor(np.min(x_obs)), np.ceil(np.max(x_obs)), 100), np.linspace(np.floor(np.min(y_obs)), np.ceil(np.max(y_obs)), 100))
    
    # Interpolation 
    #z__grd_int = interp_ppv(x_obs, y_obs, z_obs, x_grd, y_grd)
    #z__grd_int = interp_lin(x_obs, y_obs, z_obs, x_grd, y_grd)
    #z__grd_int = interp_inv(x_obs, y_obs, z_obs, x_grd, y_grd, 1)
    
    # Visualiation de l'interpolation 
    #plot_contour_2d(x_grd, y_grd, z__grd_int, x_obs, y_obs, xlabel = 'x [m]', ylabel = 'y [m]', title = 'PPV')
    #plot_surface_2d(x_grd, y_grd, z__grd_int, x_obs, y_obs, xlabel = 'x [m]', ylabel = 'y [m]', title = 'PPV')
    
    
    
    #Implémentation du variogramme empirique
    gamma_h, lst_h, ecart_carres, dist_ecart_carres = variog_empirique(x_obs, y_obs, z_obs, x_grd, y_grd)
    plot_variogramme(lst_h, gamma_h, ecart_carres, dist_ecart_carres)

    
    
    plt.show()
