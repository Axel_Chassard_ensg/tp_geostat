# -*- coding: utf-8 -*-
"""
Géostatistiques - TP Noté
Axel Chassard - Nathan Wolff
"""

### Modules ###

import numpy as np
import matplotlib.pyplot as plt
import scipy.spatial as sci

############## -- Fonctions d'interpolation -- ##############

##### -- Méthodes déterministes -- #####

def interp_ppv(x_obs, y_obs, z_obs, x_int, y_int):
    # Interpolation par plus proche voisin (reprise du code de Pierre Bosser)
    # x_obs, y_obs, z_obs : observations
    # [np.array dimension 1*n]
    # x_int, y_int, positions pour lesquelles on souhaite interpoler une valeur z_int
    # [np array dimension m*p]
    
    z_int = np.nan*np.zeros(x_int.shape)
    for i in np.arange(0,x_int.shape[0]):
        for j in np.arange(0,x_int.shape[1]):
            z_int[i,j] = z_obs[np.argmin(np.sqrt((x_int[i,j]-x_obs)**2+(y_int[i,j]-y_obs)**2))]
    return z_int

## ----------------------------- ##
    
def interp_inv(x_obs, y_obs, z_obs, x_int, y_int, p, voisi = -1):
    # Interpolation par inverse de la distance
    # x_obs, y_obs, z_obs : observations
    # [np.array dimension 1*n]
    # x_int, y_int, positions pour lesquelles on souhaite interpoler une valeur z_int
    # [np array dimension m*p]
    #p : la puissance à laquelle on souhaite faire l'interpolation 
    #voisi : le nombre de points choisis pour l'interpolation
    
    z_int = np.nan*np.zeros(x_int.shape)
    for i in np.arange(0,x_int.shape[0]):
        for j in np.arange(0,x_int.shape[1]):
            
            #Calcul de toutes les distances
            dist = np.sqrt(np.absolute((x_int[i,j]-x_obs)**p+(y_int[i,j]-y_obs)**p))**1/p
            if voisi != -1: #si on a réduit à un voisinage
                #Réduction aux "voisi" plus petites distances
                voisinage = dist.argsort(axis = 0)[:voisi]
                dist = dist[voisinage]
                s_poids = np.sum(1/dist)
                s_obs = np.sum(z_obs[voisinage]/dist)
            else:
                s_poids = np.sum(1/dist)
                s_obs = np.sum(z_obs/dist)
            
            z_int[i,j] = s_obs/s_poids
            
    return z_int

## ----------------------------- ##
    
def interp_lin(x_obs, y_obs, z_obs, x_int, y_int):
    # Interpolation linéaire par triangulation de Delaunay
    # x_obs, y_obs, z_obs : observations
    # [np.array dimension 1*n]
    # x_int, y_int, positions pour lesquelles on souhaite interpoler une valeur z_int
    # [np array dimension m*p]
    
    z_int = np.nan*np.zeros(x_int.shape)
    tr_delaunay = sci.Delaunay(np.hstack( (x_obs, y_obs) ))
    for i in np.arange(0,x_int.shape[0]):
        for j in np.arange(0,x_int.shape[1]):
            #Récupération du triangle correspondant
            idx_t = tr_delaunay.find_simplex( np.array([x_int[i,j], y_int[i,j]]) )
            if(idx_t == -1):
                continue
            
            #Récupération des sommets du triangle correspondant
            idx_s = tr_delaunay.simplices[idx_t,:]
            x1 = x_obs[ idx_s[0] ][0]
            y1 = y_obs[ idx_s[0] ][0]
            z1 = z_obs[idx_s[0]][0]
            x2 = x_obs[ idx_s[1] ][0] 
            y2 = y_obs[ idx_s[1] ][0]
            z2 = z_obs[idx_s[1]][0]
            x3 = x_obs[ idx_s[2] ][0] 
            y3 = y_obs[ idx_s[2] ][0]
            z3 = z_obs[idx_s[2]][0]
            #Détermination et résolution du système
            X = np.array([[x1,y1,1],[x2,y2,1],[x3,y3,1]])
            Y = np.array([z1,z2,z3])
            coef = np.linalg.solve(X,Y)
            #Affectation de la valeur par combinaison linéaire
            z_int[i,j] = coef[0]*x_int[i,j]+coef[1]*y_int[i,j]+coef[2]
    return z_int

## ----------------------------- ##

def fonction_phi(x_a,y_a,x_b,y_b):
    #Calcul de la fonction phi pour l'interpolation par spline
    return ((x_a-x_b)**2+(y_a-y_b)**2)*np.log(np.sqrt((x_a-x_b)**2+(y_a-y_b)**2))

## ----------------------------- ##

def interp_spl(x_obs, y_obs, z_obs, x_int, y_int, rho):
    # Interpolation par spline
    # x_obs, y_obs, z_obs : observations
    # [np.array dimension 1*n]
    # x_int, y_int, positions pour lesquelles on souhaite interpoler une valeur z_int
    # [np array dimension m*p]
    #rho : paramètre de lissage
    
    z_int = np.nan*np.zeros(x_int.shape)
    
    #Création de la matrice A
    A = np.hstack([np.ones((x_obs.shape[0],1)),x_obs,y_obs])
    for i in np.arange(0,x_obs.shape[0]):
        V = np.nan*np.zeros(x_obs.shape)
        for j in np.arange(0,x_obs.shape[0]):
            if(i==j):
                V[j,0] = np.array([[rho]])
            else : 
                V[j,0] = np.array([[fonction_phi(x_obs[i,0],y_obs[i,0],x_obs[j,0],y_obs[j,0])]])
        A = np.hstack((A,V))
    l01 = np.hstack((np.zeros((1,3)),np.ones((1,x_obs.shape[0]))))
    A = np.vstack((A,l01))
    l0x = np.hstack((np.zeros((1,3)),x_obs.reshape(1,x_obs.shape[0])))
    l0y = np.hstack((np.zeros((1,3)),y_obs.reshape(1,x_obs.shape[0])))
    A = np.vstack((A,l0x))
    A = np.vstack((A,l0y))

    #Fin de la création de la matrice A
    
    B = np.vstack((z_obs,np.zeros((3,1))))

    
    coef = np.linalg.solve(A,B)
    
    
    for i in np.arange(0,x_int.shape[0]):
        for j in np.arange(0,x_int.shape[1]):
            somme = 0 
            for k in np.arange(0,x_obs.shape[0]):
                somme += coef[k+3,0]*fonction_phi(x_int[i,j],y_int[i,j],x_obs[k,0],y_obs[k,0])
            
            z_int[i,j] = coef[0,0]+coef[1,0]*x_int[i,j]+coef[2,0]*y_int[i,j]+somme
    
    
    return z_int

##### -- Méthodes stochastiques -- #####
    
def recup_ntil(distance,h,epsilon):
    #Récupère les distances entre points d'observation comprises dans l'intervalle [h - epsilon, h + epsilon]
    # distance : [np.array dimension n*n]
    # h : np.float64
    # epsilon : float
    
    return np.where((distance!=0) & (distance>h-epsilon) & (distance<h+epsilon) )
    
def variog_experimental(x_obs, y_obs, z_obs):
    # Calcul du variogramme expérimental à partir des observations
    # x_obs, y_obs, z_obs : observations
    # [np.array dimension 1*n]

    ecart_carre = np.nan*np.zeros((x_obs.shape[0],x_obs.shape[0]))
    distance = np.nan*np.zeros((x_obs.shape[0],x_obs.shape[0]))
    for i in np.arange(0,x_obs.shape[0]):
        for j in np.arange(0,x_obs.shape[0]):
            ecart_carre[i,j] = 0.5*(z_obs[i,0]-z_obs[j,0])**2
            distance[i,j] = np.sqrt((x_obs[i,0]-x_obs[j,0])**2+(y_obs[i,0]-y_obs[j,0])**2)
    
    #Séparation de distances en intervalles réguliers
    no_zero = np.where(distance > 0)
    mini_h = int((np.min(distance[no_zero[0],no_zero[1]])))
    maxi_h = int(np.floor(np.max(distance)))
    pas_h = (maxi_h - mini_h)/1000
    lst_h = np.linspace(mini_h,maxi_h,int((maxi_h-mini_h)/pas_h) + 1)
    
    epsilon = pas_h/2
    gamma = np.array([])
    
    for h in lst_h:
        
        #récupération des couples inclus dans l'intervalle de distance
        ntil = recup_ntil(distance,h,epsilon)
        card_ntil = ntil[0].shape[0]
        if(card_ntil == 0):
            #Cas où il n'y a pas de distance comprise dans l'intervalle : on assigne une valeur nulle par défaut
            gamma = np.hstack((gamma,np.array([0])))
            continue
    
        somme_nuee_class = 0
        
        #calcul de la nuée variographique sur cet intervalle
        for i in np.arange(0,card_ntil):
            somme_nuee_class += ecart_carre[ntil[0][i],ntil[1][i]]
        gamma = np.hstack((gamma,np.array([(1/card_ntil)*somme_nuee_class])))

    return gamma, lst_h, ecart_carre, distance
    

def variogramme_analytique_lin(lst_h, gamma_h):
    # Calcul d'un variogramme analytique linéaire à partir du variogramme expérimental
    # lst_h : [np.array dimension (1001,)] -- taille arbitraire choisi dans la fonction variog_experimental --
    # gamma_h : [np.array dimension (1001,)] -- idem --
    
    #Sélection des valeurs utiles
    hmax = 700000
    idx = np.where(lst_h<=hmax)[0]
    vario_experimental = (gamma_h[idx]).reshape((-1,1))
    A = (lst_h[idx]).reshape((-1,1))
    
    #Utilisation des moindres carrés
    Xhat = np.linalg.pinv(A.T@A)@A.T@vario_experimental
    return Xhat[0]

def formule_cubique(a, C, h):
    #Applique la formule brute du variogramme analytique cubique
    #sur toutes les valeurs h utilisées pour le variogramme expérimental
    
    #On retourne une valeur en numpy.float64 pour éviter un problème lors de l'inversion des matrices (voir variogramme_analytique_cubique)
    return np.array(C*(7*(h**2/a**2) - 8.75*(h**3/a**3) + 3.5*(h**5/a**5) - 0.75*(h**7/a**7)), dtype = "float64")

def cubique_derivee_partielle_C(a, h):
    #Calcule la dérivée partielle selon C du modèle cubique
    #On retourne une valeur en numpy.float64 pour éviter un problème lors de l'inversion des matrices (voir variogramme_analytique_cubique)
    return np.array((7*(h**2/a**2) - 8.75*(h**3/a**3) + 3.5*(h**5/a**5) - 0.75*(h**7/a**7)), dtype = "float64")

def cubique_derivee_partielle_a(a, C, h) :
    #Calcule la dérivée partielle selon a du modèle cubique
    #On retourne une valeur en numpy.float64 pour éviter un problème lors de l'inversion des matrices (voir variogramme_analytique_cubique)
    return np.array(C*(-14*(h**2/a**3) + 26.25*(h**3/a**4) - 17.5*(h**5/a**6) + 5.25*(h**7/a**8)), dtype = "float64")

def variogramme_analytique_cub(lst_h, gamma_h):
    # Calcul d'un variogramme analytique cubique à partir du variogramme expérimental
    # lst_h : [np.array dimension (1001,)] -- taille arbitraire choisi dans la fonction variog_experimental --
    # gamma_h : [np.array dimension (1001,)] -- idem --
    
    #xSélection des valeurs utiles et paramètres de moindres carrés
    a0 = 700000
    C0 = 0.0003
    epsilon = 0.001
    da, dC = epsilon + 1, epsilon + 1 #arbitraire, simplement pour lancer la boucle sans dupliquer le code
    
    #Itérations des moindres carrés
    while (da > epsilon or dC > epsilon):
        
        #Linéarisation
        B = (gamma_h - formule_cubique(a0, C0, lst_h)).reshape((-1,1))
        A = np.array([cubique_derivee_partielle_a(a0, C0, lst_h).reshape((-1,1)), cubique_derivee_partielle_C(a0, lst_h).reshape((-1,1))], dtype = "float").reshape((2,lst_h.shape[0]))
        #ici il est important que tout soit en numpy.float64, sans quoi on obtient un TypeError lors de l'appel à np.linalg.solve
        X_tild = (np.linalg.pinv(A).T)@B

        da = X_tild[0,0]
        dC = X_tild[1,0]
        a0 = a0 + da
        C0 = C0 + dC
        #print(da, dC)
        
    return a0, C0

def formule_expo(a, C, h):
    #Applique la formule brute du variogramme analytique cubique
    #sur toutes les valeurs h utilisées pour le variogramme expérimental
    return C*(1 - np.exp(-h/a))

def expo_derivee_partielle_C(a, h):
    #Calcule la dérivée partielle selon C du modèle cubique
    return 1 - np.exp(-h/a)

def expo_derivee_partielle_a(a, C, h) :
    #Calcule la dérivée partielle selon a du modèle cubique
    return ((-h*C)/a**2)*np.exp(-h/a)

def variogramme_analytique_expo(lst_h, gamma_h):
    # Calcul d'un variogramme analytique exponentiel à partir du variogramme expérimental
    # lst_h : [np.array dimension (1001,)] -- taille arbitraire choisi dans la fonction variog_experimental --
    # gamma_h : [np.array dimension (1001,)] -- idem --
    
    #xSélection des valeurs utiles et para
    a0 = 700000/3
    C0 = 0.0003*0.95
    epsilon = 0.001
    da, dC = epsilon + 1, epsilon + 1 #arbitraire, simplement pour lancer la boucle sans dupliquer le code
    
    #Itérations des moindres carrés
    while (da > epsilon or dC > epsilon):
        
        #Linéarisation
        B = (gamma_h - formule_expo(a0, C0, lst_h)).reshape((-1,1))
        A = np.array([expo_derivee_partielle_a(a0, C0, lst_h).reshape((-1,1)), expo_derivee_partielle_C(a0, lst_h).reshape((-1,1))]).reshape((2,lst_h.shape[0]))
        #print(A.shape, np.linalg.pinv(A).T.shape, B.shape)
        X_tild = (np.linalg.pinv(A).T)@B
        da = X_tild[0,0]
        dC = X_tild[1,0]
        a0 = a0 + da
        C0 = C0 + dC
        #print(da, dC)
        
    return a0, C0
         
    
def krigeage_lin(x_obs, y_obs, z_obs, x_int, y_int):
    # Interpolation par krigeage linéaire
    # x_obs, y_obs, z_obs : observations
    # [np.array dimension 1*n]
    # x_int, y_int, positions pour lesquelles on souhaite interpoler une valeur z_int
    # [np array dimension m*p]
    
    v, l, dc, dec = variog_experimental(x_obs, y_obs, z_obs)
    vana = variogramme_analytique_lin(l,v)
    
    #Calcul de la matrice des poids (189*189)
    mat_p = np.nan*np.zeros((x_obs.shape[0] + 1, y_obs.shape[0] + 1))
            
    for i in np.arange(0,mat_p.shape[0] - 1):
        for j in np.arange(0,mat_p.shape[1] - 1):
            dij = np.sqrt((x_obs[i] - x_obs[j])**2 + (y_obs[i] - y_obs[j])**2)
            mat_p[i,j] = dij*vana #cas linéaire
            
    mat_p[:,-1] = 1
    mat_p[-1,:] = 1
    mat_p[-1,-1] = 0
    
    z_int = np.nan*np.zeros(x_int.shape)
    sigma2 = np.nan*np.zeros(x_int.shape)
    for i in np.arange(0,x_int.shape[0]):
        for j in np.arange(0,x_int.shape[1]):
        
            #variogramme entre le point étudié et les observations
            var = vana * np.sqrt((x_int[i,j] - x_obs)**2 + (y_int[i,j] - y_obs)**2)
            #print(var)
            var = np.vstack((var, [1]))
            #matrice de pondération
            coefs = np.linalg.solve(mat_p,var)
            #valeur interpolée
            z_int[i,j] = np.sum(coefs[:-1]*z_obs)
            #incertitude sur la valeur
            sigma2[i,j] = np.sum(coefs[:-1]*var[:-1]) + coefs[-1]
            
    return z_int, sigma2

def krigeage_cub(x_obs, y_obs, z_obs, x_int, y_int):
    # Interpolation par krigeage cubique
    # x_obs, y_obs, z_obs : observations
    # [np.array dimension 1*n]
    # x_int, y_int, positions pour lesquelles on souhaite interpoler une valeur z_int
    # [np array dimension m*p]
    
    v, l, dc, dec = variog_experimental(x_obs, y_obs, z_obs)
    vana_a, vana_C = variogramme_analytique_cub(l,v)
    
    #Calcul de la matrice des poids (189*189)
    mat_p = np.nan*np.zeros((x_obs.shape[0] + 1, y_obs.shape[0] + 1))
            
    for i in np.arange(0,mat_p.shape[0] - 1):
        for j in np.arange(0,mat_p.shape[1] - 1):
            dij = np.sqrt((x_obs[i] - x_obs[j])**2 + (y_obs[i] - y_obs[j])**2)
            if (dij > vana_a): #cas cubique
                mat_p[i,j] = vana_C
            else:
                mat_p[i,j] = vana_C*(7*(dij**2/vana_a**2) - 8.75*(dij**3/vana_a**3) + 3.5*(dij**5/vana_a**5) -0.75*(dij**7/vana_a**7))
            
    mat_p[:,-1] = 1
    mat_p[-1,:] = 1
    mat_p[-1,-1] = 0
    
    z_int = np.nan*np.zeros(x_int.shape)
    sigma2 = np.nan*np.zeros(x_int.shape)
    for i in np.arange(0,x_int.shape[0]):
        for j in np.arange(0,x_int.shape[1]):
        
            #variogramme entre le point étudié et les observations
            d = np.sqrt((x_int[i,j] - x_obs)**2 + (y_int[i,j] - y_obs)**2)
            sup = np.where(d > vana_a)[0]
            inf = np.where(d <= vana_a)[0]
            var = np.zeros(x_obs.shape)
            var[sup] = vana_C
            var[inf] = vana_C*(7*(d[inf]**2/vana_a**2) - 8.75*(d[inf]**3/vana_a**3) + 3.5*(d[inf]**5/vana_a**5) -0.75*(d[inf]**7/vana_a**7))
            var = np.vstack((var, [1]))
            #matrice de pondération
            coefs = np.linalg.solve(mat_p,var)
            #valeur interpolée
            z_int[i,j] = np.sum(coefs[:-1]*z_obs)
            #incertitude sur la valeur
            sigma2[i,j] = np.sum(coefs[:-1]*var[:-1]) + coefs[-1]
            
    return z_int, sigma2

def krigeage_expo(x_obs, y_obs, z_obs, x_int, y_int):
    # Interpolation par krigeage exponentiel
    # x_obs, y_obs, z_obs : observations
    # [np.array dimension 1*n]
    # x_int, y_int, positions pour lesquelles on souhaite interpoler une valeur z_int
    # [np array dimension m*p]
    
    v, l, dc, dec = variog_experimental(x_obs, y_obs, z_obs)
    vana_a, vana_C = variogramme_analytique_expo(l,v)
    
    #Calcul de la matrice des poids (189*189)
    mat_p = np.nan*np.zeros((x_obs.shape[0] + 1, y_obs.shape[0] + 1))
            
    for i in np.arange(0,mat_p.shape[0] - 1):
        for j in np.arange(0,mat_p.shape[1] - 1):
            dij = np.sqrt((x_obs[i] - x_obs[j])**2 + (y_obs[i] - y_obs[j])**2)
            mat_p[i,j] = vana_C*(1 - np.exp(-dij/vana_a)) #cas expo
            
    mat_p[:,-1] = 1
    mat_p[-1,:] = 1
    mat_p[-1,-1] = 0
    
    z_int = np.nan*np.zeros(x_int.shape)
    sigma2 = np.nan*np.zeros(x_int.shape)
    for i in np.arange(0,x_int.shape[0]):
        for j in np.arange(0,x_int.shape[1]):

            #variog entre le point étudié et les observations
            d = np.sqrt((x_int[i,j] - x_obs)**2 + (y_int[i,j] - y_obs)**2)
            var = vana_C*(1 - np.exp(-d/vana_a))
            var = np.vstack((var, [1]))
            #matrice de pondération
            coefs = np.linalg.solve(mat_p,var)
            #valeur interpolée
            z_int[i,j] = np.sum(coefs[:-1]*z_obs)
            #incertitude sur la valeur
            sigma2[i,j] = np.sum(coefs[:-1]*var[:-1]) + coefs[-1]
            
    return z_int, sigma2

############## -- Fonctions de visualisation -- ##############

def plot_points(x_obs, y_obs, x_cont, y_cont, xlabel = "", ylabel = "", title = "", fileo = ""):
    #Fonction intégralement reprise du code de Pierre Bosser
    
    # Tracé des sites d'observations
    # x_obs, y_obs : observations
    # xlabel, ylabel : étiquettes des axes (facultatif)
    # title : titre (facultatif)
    # fileo : nom du fichier d'enregistrement de la figure    (facultatif)
    
    fig = plt.figure()
    ax = plt.gca()
    plt.plot(x_obs, y_obs, 'ok', ms = 1)
    plt.plot(x_cont, y_cont, color = "black", linewidth = 1, markersize = 0)
#    ax.set_xlim(0.95*min(x_obs),max(x_obs)+0.05*min(x_obs))
#    ax.set_ylim(0.95*min(y_obs),max(y_obs)+0.05*min(y_obs))
    ax.set_aspect('equal', adjustable='box')
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.title(title)
    plt.grid()
    if not fileo == "": plt.savefig(fileo,bbox_inches='tight')

## ----------------------------- ##

def plot_contour_2d(x_grd ,y_grd ,z_grd, x_obs = np.array([]) ,y_obs = np.array([]), x_cont = np.array([]), y_cont = np.array([]), xlabel = "", ylabel = "", title = "", fileo = ""):
    #Fonction reprise intégralement du code de Pierre Bosser
    
    # Tracé du champ interpolé sous forme d'isolignes
    # x_grd, y_grd, z_grd : grille de valeurs interpolées
    # x_obs, y_obs : observations (facultatif)
    # xlabel, ylabel : étiquettes des axes (facultatif)
    # title : titre (facultatif)
    # fileo : nom du fichier d'enregistrement de la figure    (facultatif)
    
    z_grd_m = np.ma.masked_invalid(z_grd)
    fig = plt.figure()
    plt.plot(x_cont, y_cont, color = "black", linewidth = 1, markersize = 0)
    plt.contour(x_grd, y_grd, z_grd_m, 100, linewidths = 0.4)
    if x_obs.shape[0]>0:
        plt.scatter(x_obs, y_obs, marker = 'o', c = 'k', s = 1)
#        plt.xlim(0.95*np.min(x_obs),np.max(x_obs)+0.05*np.min(x_obs))
#        plt.ylim(0.95*np.min(y_obs),np.max(y_obs)+0.05*np.min(y_obs))
    else:
#        plt.xlim(0.95*np.min(x_grd),np.max(x_grd)+0.05*np.min(x_grd))
#        plt.ylim(0.95*np.min(y_grd),np.max(y_grd)+0.05*np.min(y_grd))
        pass
    plt.gca().set_aspect('equal', adjustable='box')
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.title(title)
    plt.grid()
    if not fileo == "": plt.savefig(fileo,bbox_inches='tight')

## ----------------------------- ##

def plot_surface_2d(x_grd ,y_grd ,z_grd, x_obs = np.array([]) ,y_obs = np.array([]), x_cont = np.array([]), y_cont = np.array([]), minmax = [0,0], xlabel = "", ylabel = "", zlabel = "", title = "", fileo = ""):
    #Fonction reprise intégralement du code de Pierre Bosser
    
    # Tracé du champ interpolé sous forme d'une surface colorée
    # x_grd, y_grd, z_grd : grille de valeurs interpolées
    # x_obs, y_obs : observations (facultatif)
    # minmax : valeurs min et max de la variable interpolée (facultatif)
    # xlabel, ylabel, zlabel : étiquettes des axes (facultatif)
    # title : titre (facultatif)
    # fileo : nom du fichier d'enregistrement de la figure    (facultatif)
    
    from matplotlib import cm
    
    z_grd_m = np.ma.masked_invalid(z_grd)
    fig = plt.figure()
    plt.plot(x_cont, y_cont, color = "black", linewidth = 1, markersize = 0)
    if minmax[0] < minmax[-1]:
        p=plt.pcolormesh(x_grd, y_grd, z_grd_m, cmap=cm.terrain, vmin = minmax[0], vmax = minmax[-1])
    else:
        p=plt.pcolormesh(x_grd, y_grd, z_grd_m, cmap=cm.terrain)
    if x_obs.shape[0]>0:
        plt.scatter(x_obs, y_obs, marker = 'o', c = 'k', s = 1)
#        plt.xlim(0.95*np.min(x_obs),np.max(x_obs)+0.05*np.min(x_obs))
#        plt.ylim(0.95*np.min(y_obs),np.max(y_obs)+0.05*np.min(y_obs))
    else:
#        plt.xlim(0.95*np.min(x_grd),np.max(x_grd)+0.05*np.min(x_grd))
#        plt.ylim(0.95*np.min(y_grd),np.max(y_grd)+0.05*np.min(y_grd))
         pass
    plt.gca().set_aspect('equal', adjustable='box')
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.title(title)
    plt.grid()
    fig.colorbar(p,ax=plt.gca(),label=zlabel,fraction=0.046, pad=0.04)
    if not fileo == "": plt.savefig(fileo,bbox_inches='tight')
  
## ----------------------------- ##    
    
def plot_variogramme(lst_h, gamma_h, ecart_carres, dist_ecart_carres):
    # Affichage prévu pour le variogramme expérimental
    # lst_h : liste des distances entre les points d'observations de taille (1001,)
    # gamma_h : valeur du variogramme pour chaque intervalle [h - epsilon, h + epsilon] autour des distances, de taille (1001,)
    # ecart_carres : écart au carrés entre chaque observation verticale (n*n)
    # dist_ecart_carres : distances associés à ces écarts (n*n)
    
    plt.figure()
    plt.plot(dist_ecart_carres, ecart_carres, "bo")
    plt.plot(lst_h,gamma_h, color = "red")
    #plt.xlim(0.95*min(dist_ecart_carres),max(dist_ecart_carres)+0.05*min(dist_ecart_carres))
    #plt.ylim(0.95*min(ecart_carres),max(ecart_carres)+0.05*min(ecart_carres))
    plt.xlabel("Distance h")
    plt.ylabel("Valeur du variogramme gamma")
    plt.title("Nuée variographique et variogramme expérimental")
    plt.grid()
    
########## -- Validation croisée -- ##########
    
## Statistiques ##
    
def get_indicateur(vect_erreur,vect_z):
    '''
    Fonction permettant d'obtenir des statistiques sur une interpolation
    '''
    
    taille = vect_erreur.shape[0]
    V_nan = []
    for i in range(taille):
        if np.isnan(vect_erreur[i]):
            V_nan.append(i)
            
    vect_erreur = np.delete(vect_erreur,V_nan)
    vect_z = np.delete(vect_z,V_nan)
    
    erreur_moyenne = np.mean(vect_erreur)
    erreur_ecart_type = np.std(vect_erreur)
    erreur_quadratique_moyenne = np.sqrt(np.mean(vect_erreur**2))
    etendue_erreur = np.max(vect_erreur) - np.min(vect_erreur)
    #Willmott
    somme_carre_erreur = np.sum(vect_erreur**2)
    denominateur = np.sum((np.abs(vect_erreur-np.mean(vect_z)) + np.abs(vect_z-np.mean(vect_z)))**2)
    Willmott = 1-somme_carre_erreur/denominateur
    
    return np.array([erreur_moyenne,erreur_ecart_type,erreur_quadratique_moyenne,etendue_erreur,Willmott])

def evaluation(x_obs,y_obs,z_obs):
    '''
    '''
    

    
    #Dans cette partie, on se propose de comparer les différentes méthodes 
    #d'interpolation. Pour cela, on va tirer aléatoirement 150 sites et pour chacun d'entre eux, on réalise l'interpolation en se basant sur n-1 sites 
    
    #On commence par créer un ensemble de test composé de 50 sites tirés aléatoirement
    identifiant = np.arange(x_obs.shape[0]).reshape(x_obs.shape[0],1)
    ensemble_obs = np.hstack((identifiant,x_obs,y_obs,z_obs))
    #en-tête : identifiant,x,y,z,z_ppv,z_lin,z_inv,z_spl,z_krg
    ensemble_test = np.array([[np.nan,np.nan,np.nan,np.nan,np.nan,np.nan,np.nan,np.nan,np.nan]])
    
    for i in range(150) : 
        
        alea = np.random.randint(ensemble_obs.shape[0])
        #On garde le nombre alea 
        new_val = np.array([[ensemble_obs[alea,0],ensemble_obs[alea,1],ensemble_obs[alea,2],ensemble_obs[alea,3],np.nan,np.nan,np.nan,np.nan,np.nan]])
        ensemble_test = np.vstack((ensemble_test,new_val))
        ensemble_obs = np.delete(ensemble_obs,alea,0)
    
    ensemble_test = np.delete(ensemble_test,0,0)
 
    del ensemble_obs
    
    #Ensuite, on va estimer par interpolation linéaire un z 

    for ligne in ensemble_test:
        ligne[4] = interp_ppv(np.delete(x_obs,int(ligne[0]),0),np.delete(y_obs,int(ligne[0]),0),np.delete(z_obs,int(ligne[0]),0), x_obs[int(ligne[0])].reshape(1,1), y_obs[int(ligne[0])].reshape(1,1))
        ligne[5] = interp_lin(np.delete(x_obs,int(ligne[0]),0),np.delete(y_obs,int(ligne[0]),0),np.delete(z_obs,int(ligne[0]),0), x_obs[int(ligne[0])].reshape(1,1), y_obs[int(ligne[0])].reshape(1,1))
        ligne[6] = interp_inv(np.delete(x_obs,int(ligne[0]),0),np.delete(y_obs,int(ligne[0]),0),np.delete(z_obs,int(ligne[0]),0), x_obs[int(ligne[0])].reshape(1,1), y_obs[int(ligne[0])].reshape(1,1),7,10)
        ligne[7] = interp_spl(np.delete(x_obs,int(ligne[0]),0),np.delete(y_obs,int(ligne[0]),0),np.delete(z_obs,int(ligne[0]),0), x_obs[int(ligne[0])].reshape(1,1), y_obs[int(ligne[0])].reshape(1,1),100000000000)
        ligne[8] = krigeage_lin(np.delete(x_obs,int(ligne[0]),0),np.delete(y_obs,int(ligne[0]),0),np.delete(z_obs,int(ligne[0]),0), x_obs[int(ligne[0])].reshape(1,1), y_obs[int(ligne[0])].reshape(1,1))[0]
        ligne[9] = krigeage_cub(np.delete(x_obs,int(ligne[0]),0),np.delete(y_obs,int(ligne[0]),0),np.delete(z_obs,int(ligne[0]),0), x_obs[int(ligne[0])].reshape(1,1), y_obs[int(ligne[0])].reshape(1,1))[0]
        ligne[10] = krigeage_expo(np.delete(x_obs,int(ligne[0]),0),np.delete(y_obs,int(ligne[0]),0),np.delete(z_obs,int(ligne[0]),0), x_obs[int(ligne[0])].reshape(1,1), y_obs[int(ligne[0])].reshape(1,1))[0]
 
    
    #Enfin, on va calculer des indicateurs statistiques sur cette interpolation
    
    #On calcule une matrice d'erreur au format e_ppv,e_lin,e_inv,e_spl,e_krie
    
    erreur = np.array([ensemble_test[:,4]-ensemble_test[:,3],ensemble_test[:,5]-ensemble_test[:,3],ensemble_test[:,6]-ensemble_test[:,3],ensemble_test[:,7],ensemble_test[:,8]-ensemble_test[:,3],ensemble_test[:,9]-ensemble_test[:,3],ensemble_test[:,10]-ensemble_test[:,3]])
  

    stat_ppv = get_indicateur(erreur[0],ensemble_test[:,3])
    stat_lin = get_indicateur(erreur[1],ensemble_test[:,3])
    stat_inv = get_indicateur(erreur[2],ensemble_test[:,3])
    stat_spl = get_indicateur(erreur[3],ensemble_test[:,3])
    stat_krigeage_lin = get_indicateur(erreur[4],ensemble_test[:,3])
    stat_krigeage_cub = get_indicateur(erreur[5],ensemble_test[:,3])
    stat_krigeage_expo = get_indicateur(erreur[6],ensemble_test[:,3])
    
    
    
    print("Statistique comparative des différentes méthodes d'interpolation")
    print("")
    print("__________________________________________________________________________________________________________________________________________")
    print(" méthode d'interpolation | erreur moyenne | écart-type des erreurs | erreur quadratique moyenne | étendue de l'erreur | indice de Willmott")
    print("plus proche voisin | {} | {} | {} | {} | {} ".format(round(stat_ppv[0],3),round(stat_ppv[1],3),round(stat_ppv[2],3),round(stat_ppv[3],3),round(stat_ppv[4],3)))
    print("linéaire | {} | {} | {} | {} | {} ".format(round(stat_lin[0],3),round(stat_lin[1],3),round(stat_lin[2],3),round(stat_lin[3],3),round(stat_lin[4],3)))
    print("inverse des distances | {} | {} | {} | {} | {} ".format(round(stat_inv[0],3),round(stat_inv[1],3),round(stat_inv[2],3),round(stat_inv[3],3),round(stat_inv[4],3)))
    print("spline de lissage | {} | {} | {} | {} | {} ".format(round(stat_spl[0],3),round(stat_spl[1],3),round(stat_spl[2],3),round(stat_spl[3],3),round(stat_spl[4],3)))
    print("krigeage linéaire | {} | {} | {} | {} | {} ".format(round(stat_krigeage_lin[0],3),round(stat_krigeage_lin[1],3),round(stat_krigeage_lin[2],3),round(stat_krigeage_lin[3],3),round(stat_krigeage_lin[4],3)))
    print("krigeage cubique | {} | {} | {} | {} | {} ".format(round(stat_krigeage_cub[0],3),round(stat_krigeage_cub[1],3),round(stat_krigeage_cub[2],3),round(stat_krigeage_cub[3],3),round(stat_krigeage_cub[4],3)))
    print("krigeage exponentiel | {} | {} | {} | {} | {} ".format(round(stat_krigeage_expo[0],3),round(stat_krigeage_expo[1],3),round(stat_krigeage_expo[2],3),round(stat_krigeage_expo[3],3),round(stat_krigeage_expo[4],3)))
    print("__________________________________________________________________________________________________________________________________________")
 
########## -- MAIN -- ##########

if __name__ == "__main__":
    
    #Import des données
    cont_france = np.loadtxt("contour_france.txt")
    x_cont = cont_france[:,0].reshape((-1,1))
    y_cont = cont_france[:,1].reshape((-1,1))
    
    stations = np.loadtxt("ocnld_FR.txt")
    x_obs = stations[:,0].reshape((-1,1))
    y_obs = stations[:,1].reshape((-1,1))
    z_obs = stations[:,2].reshape((-1,1))
    
#    #Dessin du contour de la France et affichage de l'emplacement des stations
#    plot_points(x_obs, y_obs, x_cont, y_cont, "Coordonnée Est (m)", "Coordonnée Nord (m)", "Carte des stations", "carte_stations")
#    
#    #Création de la grille d'interpolation
#    x_grd, y_grd = np.meshgrid(np.linspace(np.floor(np.min(x_obs)), np.ceil(np.max(x_obs)), 100), np.linspace(np.floor(np.min(y_obs)), np.ceil(np.max(y_obs)), 100))
#    
#    #Interpolation par plus proche voisin
#    z_grd_int = interp_ppv(x_obs, y_obs, z_obs, x_grd, y_grd)
#    plot_contour_2d(x_grd, y_grd, z_grd_int, x_obs, y_obs, x_cont, y_cont, "Coordonnée Est (m)", "Coordonnée Nord (m)", "Contour interpolation PPV", "interpo_ppv_cont")
#    plot_surface_2d(x_grd, y_grd, z_grd_int, x_obs, y_obs, x_cont, y_cont, [0,0], "Coordonnée Est (m)", "Coordonnée Nord (m)", "Déformation verticale", "Interpolation PPV", "interpo_ppv")
#    
#    #Interpolation par inverse de la distance
#    puissance = 7
#    voisi = 10
#    z_grd_int = interp_inv(x_obs, y_obs, z_obs, x_grd, y_grd, puissance, voisi)
#    plot_contour_2d(x_grd, y_grd, z_grd_int, x_obs, y_obs, x_cont, y_cont, "Coordonnée Est (m)", "Coordonnée Nord (m)", "Contour interpolation par inverse de la distance", "interpo_inv_cont")
#    plot_surface_2d(x_grd, y_grd, z_grd_int, x_obs, y_obs, x_cont, y_cont, [0,0], "Coordonnée Est (m)", "Coordonnée Nord (m)", "Déformation verticale", "Interpolation par inverse de la distance", "interpo_inv" + str(puissance))
#    
#    #Interpolation linéaire à partir d'une triangulation
#    z_grd_int = interp_lin(x_obs, y_obs, z_obs, x_grd, y_grd)
#    plot_contour_2d(x_grd, y_grd, z_grd_int, x_obs, y_obs, x_cont, y_cont, "Coordonnée Est (m)", "Coordonnée Nord (m)", "Contour interpolation linéaire par triangulation", "interpo_lin_cont")
#    plot_surface_2d(x_grd, y_grd, z_grd_int, x_obs, y_obs, x_cont, y_cont, [0,0], "Coordonnée Est (m)", "Coordonnée Nord (m)", "Déformation verticale", "Interpolation linéaire par triangulation", "interpo_lin")
#    
#    #Interpolation par spline
#    rho = 100000000000
#    z_grd_int = interp_spl(x_obs, y_obs, z_obs, x_grd, y_grd, rho)
#    plot_contour_2d(x_grd, y_grd, z_grd_int, x_obs, y_obs, x_cont, y_cont, "Coordonnée Est (m)", "Coordonnée Nord (m)", "Contour interpolation par spline", "interpo_spl" + str(rho) + "_cont")
#    plot_surface_2d(x_grd, y_grd, z_grd_int, x_obs, y_obs, x_cont, y_cont, [0,0], "Coordonnée Est (m)", "Coordonnée Nord (m)", "Déformation verticale", "Interpolation par spline", "interpo_spl" + str(rho))
#    
#    #Affichage du variogamme expérimental (intervalles réguliers de longueur 1000m, soit 1001 intervalles)
#    gamma_h, lst_h, ecart_carres, d_ecart_carres = variog_experimental(x_obs, y_obs, z_obs)
#    plot_variogramme(lst_h, gamma_h, ecart_carres, d_ecart_carres)
#
#    #Krigeage linéaire
#    z_grd_int, inc_z  = krigeage_lin(x_obs, y_obs, z_obs, x_grd, y_grd)
#    plot_contour_2d(x_grd, y_grd, z_grd_int, x_obs, y_obs, x_cont, y_cont, "Coordonnée Est (m)", "Coordonnée Nord (m)", "Contour Krigeage linéaire", "krigeage_lin_cont")
#    plot_surface_2d(x_grd, y_grd, z_grd_int, x_obs, y_obs, x_cont, y_cont, [0,0], "Coordonnée Est (m)", "Coordonnée Nord (m)", "Déformation verticale", "Krigeage linéaire", "krigeage_lin")
#    
#    #Krigeage cubique
#    z_grd_int, inc_z = krigeage_cub(x_obs, y_obs, z_obs, x_grd, y_grd)
#    plot_contour_2d(x_grd, y_grd, z_grd_int, x_obs, y_obs, x_cont, y_cont, "Coordonnée Est (m)", "Coordonnée Nord (m)", "Contour Krigeage cubique", "krigeage_cub_cont")
#    plot_surface_2d(x_grd, y_grd, z_grd_int, x_obs, y_obs, x_cont, y_cont, [0,0], "Coordonnée Est (m)", "Coordonnée Nord (m)", "Déformation verticale", "Krigeage cubique", "krigeage_cub")
#    
#    #Krigeage exponentiel
#    z_grd_int, inc_z = krigeage_expo(x_obs, y_obs, z_obs, x_grd, y_grd)
#    plot_contour_2d(x_grd, y_grd, z_grd_int, x_obs, y_obs, x_cont, y_cont, "Coordonnée Est (m)", "Coordonnée Nord (m)", "Contour Krigeage exponentiel", "krigeage_expo_cont")
#    plot_surface_2d(x_grd, y_grd, z_grd_int, x_obs, y_obs, x_cont, y_cont, [0,0], "Coordonnée Est (m)", "Coordonnée Nord (m)", "Déformation verticale", "Krigeage exponentiel", "krigeage_expo")
#    
#    
    
    evaluation(x_obs,y_obs,z_obs)
